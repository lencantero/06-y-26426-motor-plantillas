-- INSERT DE LA PAGINA DE INICIO
--INSERT INTO Inicio (titulo, url) VALUES
--//('Bienvenidos al Grugo FSOCIETY', 'assets/images/logo.jpeg');
-- INSERT DE LOS DATOS DE LOS INTEGRANTES
--INSERT INTO Integrantes (matricula, nombre, apellido, url, orden) VALUES
--('UG0507', 'Marco', 'Ortigoza', 'integrante/UG0507',1),
--('Y12858', 'Elvio', 'Martinez', 'integrante/Y12858',2),
--('Y17825', 'Alexis', 'Duarte', 'integrante/Y17825',3),
--('Y23865', 'Gabriel', 'Garcete', 'integrante/Y23865',4);

-- INSERT DEL TIPOMEDIA

--INSERT INTO TipoMedia (nombre, orden) VALUES
--('Imagen', 1),
--('Youtube', 2),
--('Dibujo', 3);

-- INSERT DE LOS DATOS DE MEDIA DE LOS INTEGRANTES
INSERT INTO Media (titulo, nombre, url_video, url_imagen, matricula, id_tipo_media, orden) VALUES 
('Video Favorito de Youtube', 'Youtube', 'https://www.youtube.com/embed/b8-tXG8KrWs?si=qBWFL28iK9JTU3JZ', '', 'Y26426', 2, 1),
('Imagen que me representa', 'imagen', '', '/assets/images/imagen-56.jpeg', 'Y26426', 1, 2),
('Dibujo en Paint', 'dibujo', '', '../../assets/images/imagen-57.png', 'Y26426', 3, 3),

('Video Favorito de Youtube', 'Youtube', 'https://www.youtube.com/embed/RW75cGvO5xY', '', 'UG0507', 2, 4),
('Imagen representativa', 'Imagen', '', '/assets/images/saturno.jpg', 'UG0507', 1, 5),
('terere', 'Dibujo', '', '../../assets/images/TERERE.png', 'UG0507', 3, 6),

('Video favorito de youtube', 'Youtube', 'https://www.youtube.com/embed/VhoHnKuf-HI', '', 'Y12858', 2, 7),
('Imagen representativa', 'Imagen', '', '/assets/images/melissa.jpg', 'Y12858', 1, 8),
('Dibujo en paint', 'Dibujo', '', '../../assets/images/dibujo-Elvio.png', 'Y12858', 3, 9),

('Video favorito de youtube', 'Youtube', 'https://www.youtube.com/embed/U1ivmi3_IeI?si=9QjM1bJzS3uUpMYk', '', 'Y17825', 2, 10),
('Imagen que representa mi personalidad.', 'Imagen', '', '/assets/images/imagen-personalidad.jpg', 'Y17825', 1, 11),
('Dibujo en paint', 'Dibujo', '', '../../assets/images/dibujo-sistema-solar.png', 'Y17825', 3, 12),

('Video favorito de youtube', 'Youtube', 'https://www.youtube.com/embed/B4LvDiIi128?rel=0', '', 'Y23865', 2, 13),
('Imagen representativa', 'Imagen', '', '/assets/images/messi_pou.jpeg', 'Y23865', 1, 14),
('Dibujo en paint', 'Dibujo', '', '../../assets/images/paint_garcete.jpg', 'Y23865', 3, 15);



