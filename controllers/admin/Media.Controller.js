const express = require("express");
const {db} = require("../../db/conexion");
const { getALL} = require ("../../db/conexion");
const { EMPTY } = require("sqlite3");


const fs = require("fs");
const multer = require("multer");
const upload = multer({ dest: "../../public/assets/images/" });
const fileUpload = upload.single("url_imagen");


const MediaController ={
    index: async function (req, res){
        const media = await getALL("SELECT * FROM Media WHERE activo = 1");
        console.log(media)
        res.render("admin/Media/index", 
        {
                media: media
        }
        );
    },
    create: async function (req, res){
        const sql = "SELECT matricula FROM integrantes WHERE activo = 1";
        const mensaje = req.query.mensaje;
        db.all(sql, (err, matricula) => {
            if (err) {
                console.error(err.message);
                res.status(500).send("Error interno del servidor");
                return;
            }
    
            res.render("admin/Media/crearForm", {
                mensaje: mensaje,
                matricula: matricula
            });
        });
    },
        
    update(req, res) {
        const idMedia = parseInt(req.params.idMedia);
        console.log("idMedia", idMedia);
        console.log("En la edicion", req.body);
    
        const imageUrl = req.file ? `public/assets/images/${req.file.filename}` : '';
    
        db.run(
            "UPDATE Media SET titulo = ?, nombre = ?, url_video =?, url_imagen = ?, matricula = ? WHERE id = ?",
            [
                req.body.titulo,
                req.body.nombre,
                req.body.url_video,
                imageUrl,
                req.body.matricula,
                idMedia,
            ],
            (err) => {
                if (err) {
                    console.error("Error al actualizar el dato Media:", err);
                    return res.status(500).send("Error interno del servidor");
                }
                res.redirect('/admin/Media/listar');
            }
        );
    },
    

     edit(req, res) {
        const idMedia = parseInt(req.params.idMedia);
        console.log("idMedia", idMedia);
        
        db.get(
            "SELECT * FROM Media WHERE id = ?",
            [idMedia],
            (err, Media) => {
                if (err) {
                    console.error("Ocurrió un error al obtener los datos de Media", err.message);
                    return res.status(500).send("Error interno del servidor");
                }
    
                if (!Media) {
                    return res.status(404).send("Datos de Media no encontrados");
                }
    
                res.render("admin/Media/editForm", {
                    Media: Media,
                });
            }
        );
    },
    destroy(req, res) {
        const idMedia = parseInt(req.params.idMedia);
        console.log("idMedia", idMedia);
        db.run(
            "UPDATE Media SET activo = 0 WHERE id = ?",
            [idMedia],
            (err) => {
                if (err) {
                    console.error("Error al eliminar el Media:", err);
                    return res.redirect('/admin/Media/listar?mensagge=Error al eliminar el Media');
                }
                console.log(`Tipo Media con ID ${idMedia} eliminado.`);
                return res.redirect('/admin/Media/listar?mensagge=La Media fue eliminada exitosamente');
            }
        )
    },
 
}


module.exports = MediaController;