const express = require("express");
const router = express.Router();
const { getALL} = require ("../db/conexion");
const {db} = require("../db/conexion");
const { EMPTY } = require("sqlite3");



const fs = require("fs");
const multer = require("multer");
const upload = multer({ dest: "./public/assets/images/"});
const fileUpload = upload.single("url_imagen"); //nombre de la columna imagen

//RUTAS DE LOS CONTROLADORES
const AdminIntegrantesController = require ("../controllers/admin/Integrantes.Controller");
const AdminTipoMediaController = require ("../controllers/admin/TipoMedia.controller");
const AdminMediaController = require("../controllers/admin/Media.Controller")


//INTEGRANTES
router.get("/integrantes/listar", AdminIntegrantesController.index);
router.get("/integrantes/crear",AdminIntegrantesController.create);
router.post("/integrantes/create", AdminIntegrantesController.store);
router.post("/integrantes/delete/:idIntegrante", AdminIntegrantesController.destroy);
router.get("/integrantes/edit/:idIntegrante", AdminIntegrantesController.edit);
router.post("/integrantes/update/:idIntegrante", AdminIntegrantesController.update);

//TipoMedia
router.get("/TipoMedia/listar", AdminTipoMediaController.index);
router.get("/TipoMedia/crear", AdminTipoMediaController.create);
router.post("/TipoMedia/create", AdminTipoMediaController.store);
router.post("/TipoMedia/delete/:idTipoMedia", AdminTipoMediaController.destroy);
router.get("/TipoMedia/edit/:idTipoMedia", AdminTipoMediaController.edit);
router.post("/TipoMedia/update/:idTipoMedia", AdminTipoMediaController.update);

//MEDIA
router.get("/Media/listar", AdminMediaController.index);
router.get("/Media/crear", AdminMediaController.create);
//router.post("/Media/create", AdminMediaController.store);
router.post("/Media/delete/:idMedia", AdminMediaController.destroy);
router.get("/Media/edit/:idMedia", AdminMediaController.edit);
router.post("/Media/update/:idMedia",fileUpload, AdminMediaController.update);


router.get("/", (req, res)=>{
    res.render("admin/index")
    
});



router.post('/Media/create', fileUpload, (req, res) => {
    if (!req.body.matricula || req.body.matricula.trim() === '') {
        return res.redirect('/admin/Media/crear?mensaje="El campo matricula no puede estar vacio. Por favor Verifique"!');
    }

    db.get(
        "SELECT MAX(orden) + 1 AS orden FROM Media",
        [],
        (err, resp) => {
            if (err) {
                console.log("error", err);
            } else {
                const imageUrl = req.file ? `/assets/images/${req.file.filename}` : '';

                db.run(
                    "INSERT INTO Media(titulo, nombre, url_video, url_imagen, matricula, id_tipo_media, orden) VALUES (?, ?, ?, ?, ?, ?, ?)",
                    [
                        req.body.titulo,
                        req.body.nombre,
                        req.body.url_video,
                        imageUrl,
                        req.body.matricula,
                        req.body.id_tipo_media,
                        resp.orden,
                    ],
                    (err) => {
                        if (err) {
                            console.log("error", err);
                        } else {
                            res.redirect("/admin/Media/listar");
                        }
                    }
                );
            }
        }
    );
});





module.exports = router; 


