
-- Tabla para almacenar los medios
CREATE TABLE IF NOT EXISTS Media (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    titulo VARCHAR(45),
    nombre VARCHAR(45),
    url_imagen VARCHAR(45)
    url_video VARCHAR(45),
    matricula VARCHAR(20) NOT NULL,
    id_tipo_media INT NOT NULL,
    orden INTEGER,
    FOREIGN KEY (matricula) REFERENCES Integrantes(matricula),
    FOREIGN KEY (id_tipo_media) REFERENCES TipoMedia(id_tipo_media)
);


-- Índices
--CREATE INDEX IF NOT EXISTS idx_tipo_media ON Media (id_tipo_media);
--CREATE INDEX IF NOT EXISTS idx_media_integrante ON Media (matricula);
