const express = require("express");
const router = express.Router();
const { getALL} = require ("../../db/conexion");
const {db} = require("../../db/conexion");
const { EMPTY } = require("sqlite3");
const { create } = require("hbs");
const { store } = require("./Integrantes.Controller");



const TipoMediaController = {
    index: async function (req, res) {
        const tipo_media = await getALL("SELECT * FROM TipoMedia Where activo = 1");
        console.log(tipo_media)
        res.render("admin/TipoMedia/index", 
        {
            tipo_media: tipo_media
        }
        )
    },
    create: async function(req, res){
        const mensaje = req.query.mensaje;
        res.render("admin/TipoMedia/crearForm",{
        mensaje:mensaje
        });
    
    },

    store: async function (req, res){
        if (!req.body.nombre || req.body.nombre.trim() === '')
            return res.redirect('/admin/TipoMedia/crear?mensaje="El campo Nombre no puede estar vacio. Por favor Verifique"!');
        db.get(
            "select max(orden) + 1 as orden from TipoMedia",
            [],
            (err, resp) => {
                if (err) console.log("error", err);
                else {
                    db.run(
                        "insert into TipoMedia(nombre, orden, activo) values (?, ?, ?)",
                        [
                            req.body.nombre,
                            resp.orden,
                            req.body.activo,
                        ],
                        (err) => {
                            if (err) console.log("error", err);
                            else res.redirect("/admin/TipoMedia/listar");
                        }
                    );
                }
            }
        );

    },
    update (req, res) {
        const idTipoMedia = parseInt(req.params.idTipoMedia);
        console.log("idTipoMedia", idTipoMedia);
        console.log("En la edicion", req.body);
        db.run(
            "UPDATE TipoMedia SET nombre = ? WHERE id_tipo_media = ?",
            [
                req.body.nombre,
                idTipoMedia,
            ],
            (err)=>{
                if(err) {
                    
                    console.error("Error al eliminar el Tipo de Media:", err);

                }
                res.redirect('/admin/TipoMedia/listar?mesagge=¡El Tipo Media se ha actualizado!');

            }
        );
    },

     edit(req, res) {
        const idTipoMedia = parseInt(req.params.idTipoMedia);
        console.log("idTipoMedia", idTipoMedia);
        
        db.get(
            "SELECT * FROM TipoMedia WHERE id_tipo_media = ?",
            [idTipoMedia],
            (err, TipoMedia) => {
                if (err) {
                    console.error("Ocurrió un error al obtener los integrantes", err.message);
                    return res.status(500).send("Error interno del servidor");
                }
    
                if (!TipoMedia) {
                    return res.status(404).send("Integrante no encontrado");
                }
    
                res.render("admin/TipoMedia/editForm", {
                    TipoMedia: TipoMedia,
                });
            }
        );
    },
    
   



    destroy(req, res) {
        const idTipoMedia = parseInt(req.params.idTipoMedia);
        console.log("idTipoMedia", idTipoMedia);
        db.run(
            "UPDATE TipoMedia SET activo = 0 WHERE id_tipo_media = ?",
            [idTipoMedia],
            (err) => {
                if (err) {
                    console.error("Error al eliminar el Tipo Media:", err);
                    return res.redirect('/admin/TipoMedia/listar?mensagge=Error al eliminar el Tipo Media');
                }
                console.log(`Tipo Media con ID ${idTipoMedia} eliminado.`);
                return res.redirect('/admin/TipoMedia/listar?mensagge=El Tipo Media fue eliminado exitosamente');
            }
        );
    }
}




module.exports = TipoMediaController;