//Archivo que contiene las rutas del proyecto
const express = require("express");
const router = express.Router();
//const db = require("../db/data")

const { getALL} = require ("../db/conexion");

require('dotenv').config({ path: '.env' });

// Rutas de mi proyecto
//Inicio 
 router.get("/", async (request, response) => {
        try {
            // Obtener todos los datos de la tabla "Inicio" desde la base de datos
            const inicio = await getALL("SELECT * FROM Inicio");
            console.log("DatosDeHome", inicio);
            // Obtener todos los integrantes desde la base de datos
            const integrantes = await getALL("SELECT * FROM Integrantes Where activo = 1");
    
            // Renderizar la página index con los datos obtenidos
            response.render("index", {
                inicio: inicio,
                integrantes: integrantes,
                inicio_admin: true,
                // Se traen los datos para el footer desde .env
                REPOSITORIO: process.env.REPOSITORIO,
                NOMBRE: process.env.NOMBRE,
                APELLIDO: process.env.APELLIDO,
                MATERIA: process.env.MATERIA
            });
        } catch (error) {
            // Manejo de errores
            console.error("Error al obtener los datos de inicio:", error);
            response.status(500).send("Error interno del servidor");
        }
    });
    
    router.get("/Integrante/:matricula", async (request, response) => {
        const matricula = request.params.matricula;                                     
        const integrante = await getALL(`SELECT * FROM integrantes WHERE activo = 1`);
        // Se asocia la matrícula del URL con la matrícula de la tabla media
        const datos = await getALL(`SELECT * FROM media WHERE matricula = '${matricula}'`);
        if (datos.length === 0) {
            // Si no se encuentran datos para la matrícula específica, se envía el código de estado 404
            response.status(404).render("404_error");
        } else {
            // Si se encuentran datos, se renderiza la plantilla "integrante"
            response.render("integrante", {
                data: datos,
                integrantes: integrante,    
                REPOSITORIO: process.env.REPOSITORIO,
                NOMBRE: process.env.NOMBRE,
                APELLIDO: process.env.APELLIDO,
                MATERIA: process.env.MATERIA
            });
        }
    });  
router.get("/paginas/word_cloud.html", async(request, response) => {
    const integrante = await getALL(`SELECT * FROM integrantes WHERE activo = 1`);
    response.render("word_cloud",{
        integrantes: integrante,
        REPOSITORIO: process.env.REPOSITORIO,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    })
    
});
router.get("/paginas/curso.html", async(request, response) => {
    const integrante = await getALL(`SELECT * FROM integrantes WHERE activo = 1`);
    response.render("curso",{
        integrantes: integrante,
        REPOSITORIO: process.env.REPOSITORIO,
        NOMBRE: process.env.NOMBRE,
        APELLIDO: process.env.APELLIDO,
        MATERIA: process.env.MATERIA
    });
    
});

module.exports = router;
