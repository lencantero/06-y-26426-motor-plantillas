const express = require("express");
const {db} = require("../../db/conexion");
const { getALL} = require ("../../db/conexion");
const { EMPTY } = require("sqlite3");
const { create } = require("hbs");




   
   /*index - listado
      create - formulario de creación
      store - método de guardar en la base de datos
      show - formulario de ver un registor
      update - método de editar un registro
      edit - formulario de edición 
      destroy - operación de eliminar un registro */

    //INDEX LISTADO DE INTEGRANTES en Admin/Integrantes /integrantes/listar'
    const IntegrantesController = {
        // Definición del objeto
        index: async function (req, res) {
            const sql = "SELECT * FROM Integrantes WHERE activo = 1";
            db.all(sql, (err, integrantes) => {
                if (err) {
                    console.error(err.message);
                    res.status(500).send("Error interno del servidor");
                    return;
                }
                res.render("admin/integrantes/index", {
                    integrantes: integrantes,
                });
            });
        },
        create: async function (req, res) { // Cambiar el orden de los parámetros
            const mensaje = req.query.mensaje;
            res.render("admin/integrantes/crearForm", {
                mensaje: mensaje
            });
        },
        store: async function (req, res) {
            if (!req.body.matricula || req.body.matricula.trim() === '' ||
                !req.body.nombre || req.body.nombre.trim() === '' ||
                !req.body.apellido || req.body.apellido.trim() === '' ||
                !req.body.url || req.body.url.trim() === '' ||
                req.body.activo === undefined || req.body.activo === null) {
                
                return res.redirect('/admin/integrantes/crear?mensaje=Todos los campos son obligatorios. Favor verificar');
            }
    
            db.get("SELECT MAX(orden) + 1 AS orden FROM integrantes", [], (err, resp) => {
                if (err) {
                    console.log("error", err);
                    res.status(500).send("Error interno del servidor");
                } else {
                    const nuevoOrden = resp.orden || 1; // En caso de que no haya ningún registro, empieza desde 1
                    db.run(
                        "INSERT INTO integrantes (matricula, nombre, apellido, url, orden, activo) VALUES (?, ?, ?, ?, ?, ?)",
                        [
                            req.body.matricula,
                            req.body.nombre,
                            req.body.apellido,
                            req.body.url,
                            nuevoOrden,
                            req.body.activo,
                        ],
                        (err) => {
                            if (err) {
                                console.log("error", err);
                                res.status(500).send("Error interno del servidor");
                            } else {
                                res.redirect("/admin/integrantes/listar");
                            }
                        }
                    );
                }
            });
        },

     show () {},
     update (req, res) {
        const idIntegrante = parseInt(req.params.idIntegrante);
        console.log("idIntegrante", idIntegrante);
        console.log("En la edicion", req.body);
        db.run(
            "UPDATE integrantes SET matricula = ?, nombre = ?, apellido =?, url= ? WHERE id = ?",
            [
                req.body.matricula,
                req.body.nombre,
                req.body.apellido,
                req.body.url,
                idIntegrante,
            ],
            (err)=>{
                if(err) {
                    
                    console.error("Error al eliminar el integrante:", err);

                }
                res.redirect('/admin/integrantes/listar?mesagge=¡El integrante se ha actualizado!');

            }
        );
    },

     edit(req, res) {
        const idIntegrante = parseInt(req.params.idIntegrante);
        console.log("idIntegrante", idIntegrante);
        
        db.get(
            "SELECT * FROM integrantes WHERE id = ?",
            [idIntegrante],
            (err, integrante) => {
                if (err) {
                    console.error("Ocurrió un error al obtener los integrantes", err.message);
                    return res.status(500).send("Error interno del servidor");
                }
    
                if (!integrante) {
                    return res.status(404).send("Integrante no encontrado");
                }
    
                res.render("admin/integrantes/editForm", {
                    integrante: integrante,
                });
            }
        );
    },
    
   
    
     //Eliminar registro - la eliminacion no debe de ser fisica de la db
     // si no en el fondo la eliminacion realmente editar un campo
     //que se utiliza en el borrado logico 
     destroy (req, res) {
        const idIntegrante = parseInt(req.params.idIntegrante);
        console.log("idIntegrante", idIntegrante);
        db.run(
            "UPDATE integrantes SET activo = 0 WHERE id = ?",
            [
                idIntegrante
            ],
            (err)=>{
                if(err) {
                    
                    console.error("Error al eliminar el integrante:", err);
                    return res.redirect('/admin/integrantes/listar?mensaje2=Error al eliminar el integrante');

                }
                console.log(`Integrante con ID ${idIntegrante} eliminado.`);
                res.redirect('/admin/integrantes/listar?mensaje2=El fue Integrante eliminado exitosamente');

            }
        );
    },
};
    // Exportar el controlador para que pueda ser utilizado en otras partes de la aplicación
    module.exports = IntegrantesController;

    